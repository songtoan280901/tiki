package com.fsoft.intern.handler;

import java.util.NoSuchElementException;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.fsoft.intern.model.response.DataResponse;

@ControllerAdvice
public class GlobalExceptionHandler {
	@ExceptionHandler(RuntimeException.class)
	@ResponseBody
	public DataResponse handleRunTimException(RuntimeException e) {
		return new DataResponse("400", e.getMessage(), 200);
	}

	@ExceptionHandler(NoSuchElementException.class)
	@ResponseStatus
	public DataResponse handlerNotFoundException(NoSuchElementException e) {
		return new DataResponse("400", e.getMessage(), 200);
	}
}

package com.fsoft.intern.service;

import java.util.List;


import com.fsoft.intern.model.entity.Brand;
import com.fsoft.intern.model.request.BrandRequest;

public interface BrandService {
	List<Brand> getAllBrands();
	Brand addBrand(BrandRequest dto);
	Brand updateBrand(BrandRequest dto, int id);
	public void deleteBrandById(int id);
	Brand findById(int id);
}

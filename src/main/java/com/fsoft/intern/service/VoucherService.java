package com.fsoft.intern.service;

import java.util.List;

import com.fsoft.intern.model.entity.Vouchers;

public interface VoucherService {
	List<Vouchers> findAllVoucher();

	List<Vouchers> foundVocher(String type);

	Vouchers save(Vouchers vouchers);
	
	Vouchers findbyId(int id);
	void delete(int id);
}

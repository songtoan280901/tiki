package com.fsoft.intern.service;

import java.util.List;

import com.fsoft.intern.model.entity.Cart;
import com.fsoft.intern.model.entity.CartItem;
import com.fsoft.intern.model.entity.Product;
import com.fsoft.intern.model.entity.User;
import com.fsoft.intern.model.response.CartItemResponse;

public interface CartService {
	Cart getCartByUid(User user);

	Cart saveCart(Cart cart);

	List<Cart> getCartItem(Cart cart);

	void deleteCartById(int id);

	int calCartTotal(Cart cart);

	CartItem saveCartItem(CartItem cartItem);

	CartItem getCartItemByPidAndCid(Product id, Cart cart);

	void deleteCartItem(int id);

	CartItem getCartItemById(int id);

	Cart getCartByUid(User user, Boolean status);

	CartItem getItemByIdAndCart(int id, Cart cart);

	CartItemResponse cartItemResponse(CartItem cartItem);
}

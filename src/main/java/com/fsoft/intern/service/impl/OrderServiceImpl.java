package com.fsoft.intern.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fsoft.intern.model.entity.Order;
import com.fsoft.intern.repository.OrderRepository;
import com.fsoft.intern.service.OrderService;

@Service
public class OrderServiceImpl implements OrderService {
	@Autowired
	OrderRepository orderRepository;

	@Override
	public Order findById(int id) {
		Order orderEntity = orderRepository.findById(id).get();
		if (orderEntity == null) {
			return null;
		}
		return orderEntity;
	}

	@Override
	public List<Order> getAll() {
		// TODO Auto-generated method stub
		return orderRepository.findAll();
	}

	@Override
	public Order save(Order order) {
		// TODO Auto-generated method stub
		return orderRepository.save(order);
	}

	@Override
	public void delete(int id) {
		orderRepository.deleteById(id);

	}

}

package com.fsoft.intern.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fsoft.intern.mapping.CartMapping;
import com.fsoft.intern.model.entity.Cart;
import com.fsoft.intern.model.entity.CartItem;
import com.fsoft.intern.model.entity.Product;
import com.fsoft.intern.model.entity.User;
import com.fsoft.intern.model.request.CartRequest;
import com.fsoft.intern.model.response.CartItemResponse;
import com.fsoft.intern.repository.CartItemRepository;
import com.fsoft.intern.repository.CartRepository;
import com.fsoft.intern.repository.ProductRepository;
import com.fsoft.intern.repository.UserRepository;
import com.fsoft.intern.service.CartService;

@Service
public class CartServiceImpl implements CartService {
	@Autowired
	CartRepository cartRepository;
	@Autowired
	CartMapping cartMapping;
	@Autowired
	ProductRepository productRepository;
	@Autowired
	UserRepository userRepository;
	@Autowired
	CartItemRepository cartItemRepository;

	@Override
	public Cart getCartByUid(User user) {
		Cart cart = cartRepository.findByUserAndStatus(user, true);
		if (cart == null)
			return null;
		return cart;
	}

	@Override
	public Cart saveCart(Cart cart) {
		// TODO Auto-generated method stub
		return cartRepository.save(cart);
	}

	@Override
	public List<Cart> getCartItem(Cart cart) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteCartById(int id) {
		// TODO Auto-generated method stub

	}

	@Override
	public int calCartTotal(Cart cart) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public CartItem saveCartItem(CartItem cartItem) {
		// TODO Auto-generated method stub
		return cartItemRepository.save(cartItem);
	}

	@Override
	public CartItem getCartItemByPidAndCid(Product id, Cart cart) {
		CartItem cartItem = cartItemRepository.findByProductAndCart(id, cart);
		if (cartItem == null)
			return null;
		return cartItem;
	}

	@Override
	public void deleteCartItem(int id) {
		cartItemRepository.deleteById(id);

	}

	@Override
	public CartItem getCartItemById(int id) {
		Optional<CartItem> cartItem = cartItemRepository.findById(id);
		if (cartItem == null)
			return null;
		return cartItem.get();
	}

	@Override
	public Cart getCartByUid(User user, Boolean status) {
		Cart cart = cartRepository.findByUserAndStatus(user, status);
		if (cart == null)
			return null;
		return cart;
	}

	@Override
	public CartItem getItemByIdAndCart(int id, Cart cart) {
		CartItem cartItem = cartItemRepository.findByIdAndCart(id, cart);
		if (cartItem == null)
			return null;
		return cartItem;
	}

	@Override
	public CartItemResponse cartItemResponse(CartItem cartItem) {
		return new CartItemResponse(cartItem.getId(), (cartItem.getProduct()), cartItem.getCart().getId(),
				cartItem.getQuantity());
	}

}

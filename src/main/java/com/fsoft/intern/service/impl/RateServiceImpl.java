package com.fsoft.intern.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.fsoft.intern.mapping.RateMapping;
import com.fsoft.intern.model.entity.Product;
import com.fsoft.intern.model.entity.Rate;
import com.fsoft.intern.model.entity.User;
import com.fsoft.intern.model.request.RateRequest;
import com.fsoft.intern.repository.ProductRepository;
import com.fsoft.intern.repository.RateRepository;
import com.fsoft.intern.repository.UserRepository;
import com.fsoft.intern.service.RateService;

@Service
public class RateServiceImpl implements RateService {
	@Autowired
	RateRepository rateRepository;
	@Autowired
	RateMapping rateMapping;
	@Autowired
	UserRepository userRepository;
	@Autowired
	ProductRepository productRepository;

	@Override
	public Rate saveRate(RateRequest dto, int userid) {
		Rate rate = new Rate();
		rate = rateMapping.dtotoEntity(dto);
		User user = userRepository.findById(userid).get();
		Product product = productRepository.findById(dto.getPid()).get();
		rate.setUid(user);
		rate.setPid(product);
		rateRepository.save(rate);
		product.setRate(productRepository.getAVGRating(dto.getPid()));
		productRepository.save(product);
		return rateRepository.save(rate);
	}

	@Override
	public Page<Rate> getRatetByPid(int id, int pageNumber, int pageSize) {
		// TODO Auto-generated method stub
		Pageable page = PageRequest.of(pageNumber - 1, pageSize, Sort.by("id").descending());
		return rateRepository.getRateByProductId(id, page);
	}

}

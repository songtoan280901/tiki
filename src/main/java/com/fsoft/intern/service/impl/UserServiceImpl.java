package com.fsoft.intern.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fsoft.intern.mapping.EditUserMapping;
import com.fsoft.intern.model.entity.Province;
import com.fsoft.intern.model.entity.User;
import com.fsoft.intern.model.request.AddUserRequest;
import com.fsoft.intern.model.request.EditUserRequest;
import com.fsoft.intern.model.request.UpdateUserPhoneRequest;
import com.fsoft.intern.repository.ProvinceRepository;
import com.fsoft.intern.repository.UserRepository;
import com.fsoft.intern.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private EditUserMapping editUserMapping;
	@Autowired
	private ProvinceRepository provinceRepository;

	@Override
	public List<User> getAllUsers() {
		// TODO Auto-generated method stub
		return userRepository.findAll();
	}

	@Override
	public User getUserById(int id) {
		// TODO Auto-generated method stub
		return userRepository.findById(id).get();
	}

	@Override
	public User updateUser(EditUserRequest dto, int id) {
		User user = userRepository.findById(id).get();
		if (user != null) {
			User olduser = editUserMapping.userDtoToUser(dto);
			user.setBirthday(olduser.getBirthday());
			Province province = provinceRepository.findById(dto.getProvince_id()).get();
			user.setFullname(olduser.getFullname());
			user.setGender(olduser.getGender());
			return userRepository.save(user);
		} else {
			throw new RuntimeException("Error: Cập nhật không thành công");
		}

	}

	@Override
	public void deleteUserbyId(int id) {
		userRepository.deleteById(id);
	}

	@Override
	public User getUserByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User saveUser(AddUserRequest user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean existbyId(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public User updatePhonenumber(int id, UpdateUserPhoneRequest phone) {
		User user = userRepository.findById(id).get();
		if (user != null) {
			if (userRepository.existsByPhone(phone.getPhone())) {
				throw new RuntimeException("Error: Số điện thoại đã tồn tại.");
			} else {
				user.setPhone(phone.getPhone());
				return userRepository.save(user);
			}
		} else {
			throw new RuntimeException("Error: Cập nhật số điện thoại không thành công");
		}
	}
}

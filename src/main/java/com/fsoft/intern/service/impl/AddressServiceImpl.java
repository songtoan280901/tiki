package com.fsoft.intern.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.fsoft.intern.mapping.AddressMapping;
import com.fsoft.intern.model.entity.Address;
import com.fsoft.intern.model.entity.Commune;
import com.fsoft.intern.model.entity.District;
import com.fsoft.intern.model.entity.Province;
import com.fsoft.intern.model.entity.User;
import com.fsoft.intern.model.request.AddressRequest;
import com.fsoft.intern.repository.AddressRepository;
import com.fsoft.intern.repository.CommuneRepository;
import com.fsoft.intern.repository.DistrictRepository;
import com.fsoft.intern.repository.ProvinceRepository;
import com.fsoft.intern.repository.UserRepository;
import com.fsoft.intern.service.AddressService;

@Service
public class AddressServiceImpl implements AddressService {
	@Autowired
	private AddressRepository addressRepository;
	@Autowired
	private ProvinceRepository provinceRepository;
	@Autowired
	private DistrictRepository districtRepository;
	@Autowired
	private CommuneRepository communeRepository;
	@Autowired
	private AddressMapping addressMapping;
	@Autowired
	private UserRepository userRepository;

	@Override
	public List<Address> listAllbyUid(int uid) {
		// TODO Auto-generated method stub
		return addressRepository.listByUid(uid);
	}

	@Override
	public Address addAddress(AddressRequest request, int uid) {
		Province province = provinceRepository.findById(request.getProvince_id()).get();
		District district = districtRepository.findById(request.getCommune_id()).get();
		Commune commune = communeRepository.findById(request.getCommune_id()).get();
		Address address = addressMapping.DTOtoAdd(request);
		User user = userRepository.findById(uid).get();
		if (province != null && district != null && commune != null) {
			if (commune.getDistrict().getId() == district.getId()
					&& district.getProvince().getId() == province.getId()) {
				address.setCommune(commune);
				address.setDistrict(district);
				address.setProvince(province);
				address.setUser(user);
				return addressRepository.save(address);
			} else
				throw new RuntimeException("Error: Dia chi khong hop le");
		} else
			throw new RuntimeException("Error: Them dia chi that bai");
	}

	@Override
	public Address editAddress(int id, AddressRequest request) {
		Address address = addressRepository.findById(id).get();
		if (address != null) {
			address.setDetail(request.getDetail());
			address.setFullname(request.getFullname());
			address.setPhone(request.getPhone());
			Province province = provinceRepository.findById(request.getProvince_id()).get();
			District district = districtRepository.findById(request.getCommune_id()).get();
			Commune commune = communeRepository.findById(request.getCommune_id()).get();
			if (province != null && district != null && commune != null) {
				if (commune.getDistrict().getId() == district.getId()
						&& district.getProvince().getId() == province.getId()) {
					address.setCommune(commune);
					address.setDistrict(district);
					address.setProvince(province);
					return addressRepository.save(address);
				} else
					throw new RuntimeException("Error: Dia chi khong hop le");
			} else
				throw new RuntimeException("Error: Dia chi khong duoc de trong");
		} else
			throw new RuntimeException("Error: Chinh sua that bai");
	}

	@Override
	public Address findById(int id) {
		// TODO Auto-generated method stub
		return addressRepository.findById(id).get();
	}
}

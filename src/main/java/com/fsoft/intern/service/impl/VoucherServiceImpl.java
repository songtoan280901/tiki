package com.fsoft.intern.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fsoft.intern.model.entity.Vouchers;
import com.fsoft.intern.repository.VoucherRepository;
import com.fsoft.intern.service.VoucherService;

@Service
public class VoucherServiceImpl implements VoucherService {
	@Autowired
	private VoucherRepository repository;

	@Override
	public List<Vouchers> findAllVoucher() {
		// TODO Auto-generated method stub
		return repository.findAll();

	}

	@Override
	public List<Vouchers> foundVocher(String type) {
		// TODO Auto-generated method stub
		return repository.findByType(type);
	}

	@Override
	public Vouchers save(Vouchers vouchers) {
		// TODO Auto-generated method stub
		return repository.save(vouchers);
	}

	@Override
	public Vouchers findbyId(int id) {
		// TODO Auto-generated method stub
		return repository.findById(id).get();
	}

	@Override
	public void delete(int id) {
	repository.deleteById(id);
		
	}

}

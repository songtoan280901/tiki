package com.fsoft.intern.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fsoft.intern.mapping.BrandMapping;
import com.fsoft.intern.model.entity.Brand;
import com.fsoft.intern.model.request.BrandRequest;
import com.fsoft.intern.repository.BrandRepository;
import com.fsoft.intern.service.BrandService;
@Service
public class BrandServiceImpl implements BrandService {

	@Autowired
	BrandRepository brandRepository;
	@Autowired
	BrandMapping brandMapping;

	@Override
	public List<Brand> getAllBrands() {
		// TODO Auto-generated method stub
		return brandRepository.findAll();
	}

	@Override
	public Brand addBrand(BrandRequest dto) {
		// TODO Auto-generated method stub
		return brandRepository.save(brandMapping.dtoToEntiry(dto));
	}

	@Override
	public Brand updateBrand(BrandRequest dto, int id) {
		// TODO Auto-generated method stub
		Brand brand = brandRepository.findById(id).get();
		if (brand != null) {
			brand.setName(dto.getName());
			return brandRepository.save(brand);
		} else {
			return null;
		}
	}

	@Override
	public void deleteBrandById(int id) {
		brandRepository.deleteById(id);

	}

	@Override
	public Brand findById(int id) {
		// TODO Auto-generated method stub
		return brandRepository.findById(id).get();
	}

}

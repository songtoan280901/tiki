package com.fsoft.intern.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fsoft.intern.mapping.ProvinceMapping;
import com.fsoft.intern.model.entity.Province;
import com.fsoft.intern.model.request.ProvinceRequest;
import com.fsoft.intern.repository.ProvinceRepository;
import com.fsoft.intern.service.ProvinceService;
@Service
public class ProvinceServiceImpl implements ProvinceService {
	@Autowired
	ProvinceRepository provinceRepository;
	@Autowired
	ProvinceMapping provinceMapping;

	@Override
	public List<Province> listAllProvinces() {
		// TODO Auto-generated method stub
		return provinceRepository.findAll();
	}

	@Override
	public Province findById(int id) {
		// TODO Auto-generated method stub
		return provinceRepository.findById(id).get();
	}

	@Override
	public Province addProvince(ProvinceRequest dto) {
		// TODO Auto-generated method stub
		return provinceRepository.save(provinceMapping.dtoToEntity(dto));
	}

	@Override
	public Province updateProvince(ProvinceRequest dto, int id) {
		Province province = provinceRepository.findById(id).get();
		if (province != null) {
			province.setName(dto.getName());
			return provinceRepository.save(province);
		} else {
			throw new RuntimeException("Error: Cap nhat that bai");
		}
	}

	@Override
	public void deleteProvinceById(int id) {
		provinceRepository.deleteById(id);

	}

}

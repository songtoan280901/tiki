package com.fsoft.intern.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fsoft.intern.mapping.AddCategoryMapping;
import com.fsoft.intern.model.entity.CartItem;
import com.fsoft.intern.model.entity.Category;
import com.fsoft.intern.model.request.AddCategoryRequest;
import com.fsoft.intern.repository.CategoryRepository;
import com.fsoft.intern.service.CategoryService;

@Service
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	private CategoryRepository categoryRepository;
	@Autowired
	private AddCategoryMapping categoryMapping;

	@Override
	public Category addCategory(AddCategoryRequest dto) {
		return categoryRepository.save(categoryMapping.DTOtoCate(dto));
	}

	@Override
	public Category updateCategory(int id, AddCategoryRequest dto) {
		Category category = categoryRepository.findById(id);
		if (category != null) {
			category.setName(dto.getName());
			return categoryRepository.save(category);
		} else {
			throw new RuntimeException("Error: Cap nhat danh muc that bai");
		}
	}

	@Override
	public boolean deleteCategory(int id) {
		if (id >= 1) {
			Category category = categoryRepository.findById(id);
			if (category != null) {
				categoryRepository.delete(category);
				return true;
			}

		}
		return false;
	}

	@Override
	public List<Category> getAllCategory() {
		// TODO Auto-generated method stub
		return categoryRepository.findAll();
	}

	@Override
	public Category getOneCategory(int id) {
		// TODO Auto-generated method stub
		return categoryRepository.findById(id);
	}

}

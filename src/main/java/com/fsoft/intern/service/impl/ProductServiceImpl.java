package com.fsoft.intern.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.fsoft.intern.mapping.ProductMapping;
import com.fsoft.intern.model.entity.Brand;
import com.fsoft.intern.model.entity.Category;
import com.fsoft.intern.model.entity.Product;
import com.fsoft.intern.model.entity.User;
import com.fsoft.intern.model.request.ProductRequest;
import com.fsoft.intern.model.request.SearchProductRequest;
import com.fsoft.intern.repository.CategoryRepository;
import com.fsoft.intern.repository.ProductRepository;
import com.fsoft.intern.repository.UserRepository;
import com.fsoft.intern.service.BrandService;
import com.fsoft.intern.service.ProductService;
import com.fsoft.intern.service.UserService;

@Service
public class ProductServiceImpl implements ProductService {
	@Autowired
	private ProductRepository productRepository;
	@Autowired
	private ProductMapping productMapping;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private CategoryRepository categoryRepository;
	@Autowired
	private UserService userService;
	@Autowired
	private BrandService brandService;

	@Override
	public List<Product> productSearchByName(String name) {
		// TODO Auto-generated method stub
		return productRepository.searchByName(name);
	}

	@Override
	public Product saveProduct(ProductRequest dto, int userid) {
		// TODO Auto-generated method stub
		Product pro = new Product();
		pro = productMapping.DtoToEntity(dto);
		Brand brand = brandService.findById(dto.getBrand_id());
		Category cate = categoryRepository.findById(dto.getCate_id());
		User user = userRepository.findById(userid).get();
		pro.setUser(user);
		pro.setBrand(brand);
		pro.setCategory(cate);
		return productRepository.save(pro);
	}

	@Override
	public Product updateProduct(ProductRequest dto, int id) {
		Product product = productRepository.findById(id).get();
		if (product != null) {
			product.setDescription(dto.getDescription());
			product.setImage(dto.getImage());
			product.setName(dto.getName());
			product.setOrigin(dto.getOrigin());
			product.setPrice(dto.getPrice());
			return productRepository.save(product);
		} else {
			throw new RuntimeException("Error: cap nhat that bai");
		}
	}

	@Override
	public List<Product> listProductsByCid(int cate_id) {
		// TODO Auto-generated method stub
		return productRepository.listProductsByCid(cate_id);
	}

	@Override
	public List<Product> listProductsByBid(int cate_id, int brand_id) {
		return productRepository.listProductsByBid(cate_id, brand_id);
	}

	@Override
	public List<Product> listAll() {
		// TODO Auto-generated method stub
		return productRepository.findAll();
	}

	@Override
	public Page<Product> listProductsWithPagination(int cate_id, int pageNumber, int pageSize) {
		// TODO Auto-generated method stub
		Pageable page = PageRequest.of(pageNumber - 1, pageSize);
		return productRepository.listProductsByCid(cate_id, page);
	}

	@Override
	public Page<Product> listProductsWithPaginationAndPriceASC(int cate_id, int pageNumber, int pageSize) {
		// TODO Auto-generated method stub
		Pageable page = PageRequest.of(pageNumber - 1, pageSize, Sort.by("price").ascending());
		return productRepository.listProductsByCid(cate_id, page);
	}

	@Override
	public Page<Product> listProductsWithPaginationAndPriceDESC(int cate_id, int pageNumber, int pageSize) {
		// TODO Auto-generated method stub
		Pageable page = PageRequest.of(pageNumber - 1, pageSize, Sort.by("price").descending());
		return productRepository.listProductsByCid(cate_id, page);
	}

	@Override
	public List<Product> getProductsBetween(int cate_id, int priceMin, int priceMax) {
		List<Product> products = new ArrayList<>();
		List<Product> proWithPriceFilter = new ArrayList<>();
		products = productRepository.listProductsByCid(cate_id);

		for (int i = 0; i < products.size(); i++) {
			if (priceMin < products.get(i).getPrice() && products.get(i).getPrice() < priceMax) {
				proWithPriceFilter.add(products.get(i));
			}
		}
		return proWithPriceFilter;
	}

	@Override
	public Page<Product> listNewestProductsWithPagination(int cate_id, int pageNumber, int pageSize) {
		// TODO Auto-generated method stub
		Pageable page = PageRequest.of(pageNumber - 1, pageSize, Sort.by("id").descending());
		return productRepository.listProductsByCid(cate_id, page);
	}

	@Override
	public Page<Product> listProductsByProvince(int cate_id, int province_id, int pageNumber, int pageSize) {
		Pageable page = PageRequest.of(pageNumber - 1, pageSize);
		return productRepository.listProductByProvince(cate_id, province_id, page);
	}

	@Override
	public Page<Product> listProductsByRateFrom3(int cate_id, int pageNumber, int pageSize) {
		// TODO Auto-generated method stub
		Pageable page = PageRequest.of(pageNumber - 1, pageSize);
		return productRepository.listProductByRateFrom3(cate_id, page);
	}

	@Override
	public Page<Product> listProductsByRateFrom4(int cate_id, int pageNumber, int pageSize) {
		// TODO Auto-generated method stub
		Pageable page = PageRequest.of(pageNumber - 1, pageSize);
		return productRepository.listProductByRateFrom4(cate_id, page);
	}

	@Override
	public Page<Product> listProductsByRateFrom5(int cate_id, int pageNumber, int pageSize) {
		// TODO Auto-generated method stub
		Pageable page = PageRequest.of(pageNumber - 1, pageSize);
		return productRepository.listProductByRateFrom5(cate_id, page);
	}

	@Override
	public Page<Product> listNewestProductsByCate(int cate_id, int pageNumber, int pageSize) {
		// TODO Auto-generated method stub
		Pageable page = PageRequest.of(pageNumber - 1, pageSize, Sort.by("id").descending());
		return productRepository.listProductsByCid(cate_id, page);
	}

}

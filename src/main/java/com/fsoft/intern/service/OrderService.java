package com.fsoft.intern.service;

import java.util.List;

import com.fsoft.intern.model.entity.Order;

public interface OrderService {
	Order findById(int id);
	List<Order> getAll();
	Order save(Order order);
	void delete (int id);
}

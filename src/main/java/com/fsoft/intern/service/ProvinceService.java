package com.fsoft.intern.service;

import java.util.List;

import com.fsoft.intern.model.entity.Province;
import com.fsoft.intern.model.request.ProvinceRequest;

public interface ProvinceService {
	List<Province> listAllProvinces();

	Province findById(int id);

	Province addProvince(ProvinceRequest dto);

	Province updateProvince(ProvinceRequest dto, int id);

	public void deleteProvinceById(int id);
}

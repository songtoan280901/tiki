package com.fsoft.intern.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.fsoft.intern.model.entity.Product;
import com.fsoft.intern.model.request.ProductRequest;
import com.fsoft.intern.model.request.SearchProductRequest;

public interface ProductService {
	List<Product> productSearchByName(String name);

	Product saveProduct(ProductRequest dto, int userid);

	Product updateProduct(ProductRequest dto, int id);

	List<Product> listProductsByCid(int cate_id);

	List<Product> listProductsByBid(int cate_id, int brand_id);

	List<Product> listAll();

	Page<Product> listProductsWithPagination(int cate_id, int pageNumber, int pageSize);

	Page<Product> listProductsWithPaginationAndPriceASC(int cate_id, int pageNumber, int pageSize);

	Page<Product> listProductsWithPaginationAndPriceDESC(int cate_id, int pageNumber, int pageSize);

	List<Product> getProductsBetween(int cate_id, int priceMin, int priceMax);

	Page<Product> listNewestProductsWithPagination(int cate_id, int pageNumber, int pageSize);

	Page<Product> listProductsByProvince(int cate_id, int province_id, int pageNumber, int pageSize);

	Page<Product> listProductsByRateFrom3(int cate_id, int pageNumber, int pageSize);

	Page<Product> listProductsByRateFrom4(int cate_id, int pageNumber, int pageSize);

	Page<Product> listProductsByRateFrom5(int cate_id, int pageNumber, int pageSize);

	Page<Product> listNewestProductsByCate(int cate_id, int pageNumber, int pageSize);
}

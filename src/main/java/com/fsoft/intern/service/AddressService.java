package com.fsoft.intern.service;

import java.util.List;

import com.fsoft.intern.model.entity.Address;
import com.fsoft.intern.model.request.AddressRequest;

public interface AddressService {
	List<Address> listAllbyUid(int uid);

	Address addAddress(AddressRequest request, int uid);

	Address editAddress(int id, AddressRequest request);

	Address findById(int id);

}

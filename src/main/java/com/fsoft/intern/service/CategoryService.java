package com.fsoft.intern.service;

import java.util.List;


import com.fsoft.intern.model.entity.Category;
import com.fsoft.intern.model.request.AddCategoryRequest;

public interface CategoryService {
	public Category addCategory(AddCategoryRequest dto);
	
	public Category updateCategory(int id, AddCategoryRequest dto);
	
	public boolean deleteCategory(int id);
	
	public List<Category> getAllCategory();
	
	public Category getOneCategory(int id);
}

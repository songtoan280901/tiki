package com.fsoft.intern.service;

import org.springframework.data.domain.Page;

import com.fsoft.intern.model.entity.Rate;
import com.fsoft.intern.model.request.RateRequest;

public interface RateService {
	Rate saveRate(RateRequest dto, int userid);

	Page<Rate> getRatetByPid(int id, int pageNumber, int pageSize);
}

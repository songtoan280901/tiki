package com.fsoft.intern.service;

import java.util.List;

import com.fsoft.intern.model.entity.User;
import com.fsoft.intern.model.request.AddUserRequest;
import com.fsoft.intern.model.request.EditUserRequest;
import com.fsoft.intern.model.request.UpdateUserPhoneRequest;

public interface UserService {
	List<User> getAllUsers();

	User getUserById(int id);

	User updateUser(EditUserRequest user, int id);

	void deleteUserbyId(int id);

	User getUserByName(String name);

	User saveUser(AddUserRequest user);

	boolean existbyId(int id);

	User updatePhonenumber(int id, UpdateUserPhoneRequest phone);
}

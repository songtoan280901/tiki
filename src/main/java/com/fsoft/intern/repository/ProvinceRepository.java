package com.fsoft.intern.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fsoft.intern.model.entity.Province;

@Repository
public interface ProvinceRepository  extends JpaRepository<Province, Integer>{

}

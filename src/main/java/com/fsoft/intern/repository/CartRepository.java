package com.fsoft.intern.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fsoft.intern.model.entity.Cart;
import com.fsoft.intern.model.entity.User;

public interface CartRepository extends JpaRepository<Cart, Integer> {
	Cart findByUserAndStatus(User user, Boolean status);
}

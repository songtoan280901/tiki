package com.fsoft.intern.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fsoft.intern.model.entity.Commune;

public interface CommuneRepository extends JpaRepository<Commune, Integer> {

}

package com.fsoft.intern.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fsoft.intern.model.entity.Category;
@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {
	public Category findById(int id);

	public Category findOneById(int id);
}

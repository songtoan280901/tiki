package com.fsoft.intern.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fsoft.intern.model.entity.District;

public interface DistrictRepository extends JpaRepository<District, Integer>{

}

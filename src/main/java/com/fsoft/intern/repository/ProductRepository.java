package com.fsoft.intern.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.fsoft.intern.model.entity.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {
	@Query(value = "select * from Products where name like %?1%", nativeQuery = true)
	public List<Product> searchByName(String name);

	@Query(value = "select * from Products where cate_id = ?", nativeQuery = true)
	public List<Product> listProductsByCid(int cate_id);

	@Query(value = "select * from Products where cate_id = ? and brand_id = ?", nativeQuery = true)
	public List<Product> listProductsByBid(int cate_id, int brand_id);

	@Query(value = "select * "
			+ "from Products inner join Sellers "
			+ "on Products.seller_id = Sellers.seller_id "
			+ "where Products.cate_id = ? and Sellers.province_id = ?", countQuery = "SELECT count(*)"
					+ " FROM Products inner join Sellers  "
					+ "on Products.seller_id = Sellers.seller_id "
					+ "where Products.cate_id = ? and Sellers.province_id = ? ", nativeQuery = true)
	public Page<Product> listProductByProvince(int cate_id, int province_id, Pageable pageable);

	@Query(value = "select avg(rating_point) from product_rates where product_id = ?", nativeQuery = true)
	public int getAVGRating(int id);

	@Query(value = "select * from Products where cate_id = ? and rate_point > 2", nativeQuery = true)
	public Page<Product> listProductByRateFrom3(int cate_id, Pageable pageable);

	@Query(value = "select * from Products where cate_id = ? and rate_point > 3", nativeQuery = true)
	public Page<Product> listProductByRateFrom4(int cate_id, Pageable pageable);

	@Query(value = "select * from Products where cate_id = ? and rate_point > 4", nativeQuery = true)
	public Page<Product> listProductByRateFrom5(int cate_id, Pageable pageable);

	@Query(value = "select * from Products where cate_id = ?1", nativeQuery = true)
	public Page<Product> listProductsByCid(int cate_id, Pageable pageable);

	@Query(value = "select p From Products p inner join Addresses on p.userid = Addresses.userid" + "where "
			+ "(Addresses.province_id = :provinceId OR :provinceId IS NULL) AND"
			+ "(d.rate_point >= :rating OR :rating IS NULL) AND"
			+ "(d.price >= :minprice OR :minprice IS NULL) AND"
			+ "(d.price <= :maxprice OR :maxprice IS NULL) AND"
			+ "(d.brand_id = :brandId OR :brandId IS NULL) AND", nativeQuery = true)
	List<Product> search(@Param("provinceId") int provinceid, @Param("rating") int rating,
			@Param("minprice") double minprice, @Param("maxprice") double maxprice, @Param("brandId") int brand);

}

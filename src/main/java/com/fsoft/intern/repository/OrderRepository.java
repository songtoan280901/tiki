package com.fsoft.intern.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fsoft.intern.model.entity.Order;
@Repository
public interface OrderRepository extends JpaRepository<Order, Integer> {
}

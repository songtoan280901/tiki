package com.fsoft.intern.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fsoft.intern.model.entity.Vouchers;

public interface VoucherRepository extends JpaRepository<Vouchers, Integer> {
	List<Vouchers> findByType(String type);

}

package com.fsoft.intern.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fsoft.intern.model.entity.Cart;
import com.fsoft.intern.model.entity.CartItem;
import com.fsoft.intern.model.entity.Product;

public interface CartItemRepository extends JpaRepository<CartItem, Integer> {
	CartItem findByProductAndCart(Product id, Cart cart);

	CartItem findByIdAndCart(int id, Cart cart);
}

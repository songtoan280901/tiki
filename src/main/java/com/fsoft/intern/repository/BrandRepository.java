package com.fsoft.intern.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fsoft.intern.model.entity.Brand;

public interface BrandRepository extends JpaRepository<Brand, Integer> {

}

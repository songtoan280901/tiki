package com.fsoft.intern.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.fsoft.intern.model.entity.Address;

public interface AddressRepository extends JpaRepository<Address, Integer> {

	@Query(value = "Select * from addresses where user_id = ?", nativeQuery =  true)
	List<Address> listByUid(int uid);
	
}

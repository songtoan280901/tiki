package com.fsoft.intern.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.fsoft.intern.model.entity.Rate;

public interface RateRepository extends JpaRepository<Rate, Integer> {
	@Query(value = "select * from product_rates where product_id = ?", nativeQuery = true)
	Page<Rate> getRateByProductId(int id, Pageable pageable);
}

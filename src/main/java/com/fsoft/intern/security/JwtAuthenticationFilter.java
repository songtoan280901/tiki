package com.fsoft.intern.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import io.jsonwebtoken.ExpiredJwtException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {
	@Autowired
	private CustomUserDetailsService jwtUserDetailsService;

	@Autowired
	private JwtTokenProvider tokenProvider;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		// get JWT (token) from http request
		String token = getJWTfromRequest(request);
		// validate token
		if (StringUtils.hasText(token) && tokenProvider.validateToken(token)) {
			// get username from token
			String username = tokenProvider.getUsernameFromJWT(token);
			// load user associated with token
			UserDetails userDetails = jwtUserDetailsService.loadUserByUsername(username);

			UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
					userDetails, null, userDetails.getAuthorities());
			authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
			// set spring security
			SecurityContextHolder.getContext().setAuthentication(authenticationToken);
		}

//		try {
//			// JWT Token is in the form "Bearer token". Remove Bearer word and
//			// get only the Token
//			String token = getJWTfromRequest(request);
//
//			if (StringUtils.hasText(token) && tokenProvider.validateToken(token)) {
//				UserDetails userDetails = new User(tokenProvider.getUsernameFromToken(token), "",
//						tokenProvider.getRolesFromToken(token));
//
//				UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
//						userDetails, null, userDetails.getAuthorities());
//				// After setting the Authentication in the context, we specify
//				// that the current user is authenticated. So it passes the
//				// Spring Security Configurations successfully.
//				SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
//			} else {
//				System.out.println("Cannot set the Security Context");
//			}
//		} catch (ExpiredJwtException ex) {
//			String isRefreshToken = request.getHeader("isRefreshToken");
//			String requestURL = request.getRequestURL().toString();
//			// allow for Refresh Token creation if following conditions are true.
//			if (isRefreshToken != null && isRefreshToken.equals("true") && requestURL.contains("refreshtoken")) {
//				allowForRefreshToken(ex, request);
//			} else
//				request.setAttribute("exception", ex);
//		} catch (BadCredentialsException ex) {
//			request.setAttribute("exception", ex);
//		}
		filterChain.doFilter(request, response);
	}

	// Bearer <accessToken>
	private String getJWTfromRequest(HttpServletRequest request) {
		String bearerToken = request.getHeader("Authorization");
		if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
			return bearerToken.substring(7, bearerToken.length());
		}
		return null;
	}


}

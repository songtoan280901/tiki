package com.fsoft.intern.security;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.fsoft.intern.model.entity.Role;

import com.fsoft.intern.model.entity.User;

import com.fsoft.intern.model.request.SignUpRequest;
import com.fsoft.intern.model.request.SignUpSellerRequest;
import com.fsoft.intern.repository.UserRepository;
import com.fsoft.intern.repository.CategoryRepository;
import com.fsoft.intern.repository.ProductRepository;
import com.fsoft.intern.repository.ProvinceRepository;
import com.fsoft.intern.repository.RoleRepository;
import com.fsoft.intern.model.entity.Category;
import com.fsoft.intern.model.entity.Erole;
import com.fsoft.intern.model.entity.Province;

@Service
public class CustomUserDetailsService implements UserDetailsService {
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;
	private PasswordEncoder bcryptEncoder = new BCryptPasswordEncoder();
	private Collection<? extends GrantedAuthority> authorities;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		System.out.print(userRepository.count());
		User user = userRepository.findByUsername(username).get();
		if (user == null) {
			throw new UsernameNotFoundException("User not found with username: " + username);
		} else {
			List<GrantedAuthority> authorities = user.getRoles().stream()
					.map(role -> new SimpleGrantedAuthority(role.getName().name())).collect(Collectors.toList());
			return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
					authorities);
		}
	}

	public User save(SignUpRequest user) {
		User newuser = new User();
		newuser.setUsername(user.getUsername());
		newuser.setPassword(bcryptEncoder.encode(user.getPassword()));
		newuser.setEmail(user.getEmail());
		Set<String> strRoles = user.getRole();
		Set<Role> roles = new HashSet<>();
		if (strRoles == null) {
			Role userRole = roleRepository.findByName(Erole.ROLE_USER)
					.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
			roles.add(userRole);
		} else {
			strRoles.forEach(role -> {
				switch (role) {
				case "admin":
					Role adminRole = roleRepository.findByName(Erole.ROLE_ADMIN)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(adminRole);

					break;
				default:
					Role userRole = roleRepository.findByName(Erole.ROLE_USER)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(userRole);
				}
			});
		}

		newuser.setRoles(roles);
		return userRepository.save(newuser);
	}

}

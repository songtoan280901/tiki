package com.fsoft.intern.controller;

import java.io.IOException;
import java.text.DateFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import com.fsoft.intern.model.entity.User;
import com.fsoft.intern.model.request.EditUserRequest;
import com.fsoft.intern.model.request.UpdateUserPhoneRequest;
import com.fsoft.intern.model.response.DataResponse;
import com.fsoft.intern.repository.UserRepository;
import com.fsoft.intern.security.CustomUserDetailsService;
import com.fsoft.intern.security.JwtTokenProvider;
import com.fsoft.intern.service.UserService;

@RestController
@RequestMapping("/api/v1/user")
public class UserController {
	@Autowired
	private UserService userService;
	@Autowired
	private JwtTokenProvider tokenProvider;
	@Autowired
	private UserRepository userRepository;

	@GetMapping("")
	public DataResponse getUser(HttpServletRequest httpServletRequest) {

		String AuthorizationHeader = httpServletRequest.getHeader("Authorization");
		if (AuthorizationHeader != null && AuthorizationHeader.startsWith("Bearer ")) {
			String accessToken = AuthorizationHeader.substring("Bearer ".length());
			String username = tokenProvider.getUsernameFromJWT(accessToken);
			User user = userRepository.findByUsername(username).get();
			if (user != null) {
				return new DataResponse(userService.getUserById(user.getId()));
			} else
				throw new RuntimeException("User khong ton tai");
		} else
			throw new RuntimeException("Access token missing");

	}

	@PutMapping("/account/edit")
	public DataResponse EditUserbyId(@RequestBody @Valid EditUserRequest request, HttpServletRequest httpServletRequest,
			BindingResult result) {
		if (!result.hasErrors()) {
			String AuthorizationHeader = httpServletRequest.getHeader("Authorization");
			if (AuthorizationHeader != null && AuthorizationHeader.startsWith("Bearer ")) {
				String accessToken = AuthorizationHeader.substring("Bearer ".length());
				String username = tokenProvider.getUsernameFromJWT(accessToken);
				User user = userRepository.findByUsername(username).get();
				if (user != null) {
					return new DataResponse(userService.updateUser(request, user.getId()));
				} else
					throw new RuntimeException("User khong ton tai");
			} else
				throw new RuntimeException("Access token missing");
		}
		throw new RuntimeException(result.getFieldError().toString());
	}

	@PutMapping("/account/edit/{id}/phone")
	public DataResponse EditPhone(@RequestBody @Valid UpdateUserPhoneRequest request,
			HttpServletRequest httpServletRequest, BindingResult result) {
		if (!result.hasErrors()) {
			String AuthorizationHeader = httpServletRequest.getHeader("Authorization");
			if (AuthorizationHeader != null && AuthorizationHeader.startsWith("Bearer ")) {
				String accessToken = AuthorizationHeader.substring("Bearer ".length());
				String username = tokenProvider.getUsernameFromJWT(accessToken);
				User user = userRepository.findByUsername(username).get();
				if (user != null) {
					return new DataResponse(userService.updatePhonenumber(user.getId(), request));
				} else
					throw new RuntimeException("User khong ton tai");
			} else
				throw new RuntimeException("Access token missing");
		}
		throw new RuntimeException(result.getFieldError().toString());
	}

}

package com.fsoft.intern.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fsoft.intern.model.entity.Cart;
import com.fsoft.intern.model.entity.CartItem;
import com.fsoft.intern.model.entity.Product;
import com.fsoft.intern.model.entity.User;
import com.fsoft.intern.model.request.AddItemCartRequest;
import com.fsoft.intern.model.request.CartRequest;
import com.fsoft.intern.model.request.SubItemRequest;
import com.fsoft.intern.model.response.CartItemResponse;
import com.fsoft.intern.model.response.DataResponse;
import com.fsoft.intern.repository.ProductRepository;
import com.fsoft.intern.repository.UserRepository;
import com.fsoft.intern.security.JwtTokenProvider;
import com.fsoft.intern.service.CartService;
import com.fsoft.intern.service.ProductService;
import com.fsoft.intern.service.UserService;

import net.bytebuddy.asm.MemberSubstitution.Substitution.Stubbing;

@RestController
@RequestMapping("/api/v1/cart")
public class CartController {
	@Autowired
	private CartService cartService;
	@Autowired
	private UserService userService;
	@Autowired
	private ProductService productService;
	@Autowired
	AuthenticationManager authenticationManager;
	@Autowired
	private JwtTokenProvider tokenProvider;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private ProductRepository productRepository;

	@GetMapping("")
	@PreAuthorize("hasRole('USER')")
	public DataResponse getCart(HttpServletRequest httpServletRequest) {
		String AuthorizationHeader = httpServletRequest.getHeader("Authorization");
		if (AuthorizationHeader != null && AuthorizationHeader.startsWith("Bearer ")) {
			String accessToken = AuthorizationHeader.substring("Bearer ".length());
			String username = tokenProvider.getUsernameFromJWT(accessToken);
			User user = userRepository.findByUsername(username).get();
			if (user != null) {
				Cart cart = cartService.getCartByUid(user);
				if (cart == null) {
					Cart newCart = new Cart((double) 0, user, true);
					cartService.saveCart(newCart);
					return new DataResponse("200", "Ban chua co gi trong gio hang (Add Cart)", 200);
				}
				if (cart.getCartitem().isEmpty()) {
					return new DataResponse("200", "Ban chua co gi trong gio hang", 200);
				} else {
					List<CartItem> cartItemEntityList = cart.getCartitem();
					List<CartItemResponse> cartItemResponseList = new ArrayList<>();
					for (CartItem cartItem : cartItemEntityList) {
						cartItemResponseList.add(cartService.cartItemResponse(cartItem));
					}
					return new DataResponse(cartItemResponseList);
				}
			} else
				throw new RuntimeException("User khong ton tai");
		} else
			throw new RuntimeException("Access token missing");
	}

	@PutMapping("")
	@PreAuthorize("hasRole('USER')")
	public DataResponse addtoCart(@RequestBody @Valid AddItemCartRequest request, HttpServletRequest httpServletRequest,
			BindingResult result) {
		String AuthorizationHeader = httpServletRequest.getHeader("Authorization");
		if (AuthorizationHeader != null && AuthorizationHeader.startsWith("Bearer ")) {
			String accessToken = AuthorizationHeader.substring("Bearer ".length());
			String username = tokenProvider.getUsernameFromJWT(accessToken);
			User user = userRepository.findByUsername(username).get();
			if (user != null) {
				Cart cart = cartService.getCartByUid(user);
				Product product = productRepository.findById(request.getProductId()).get();
				if (product == null) {
					return new DataResponse("400", "Khong tim thay san pham", 200);
				}
				if (cart == null) {
					cart = new Cart((double) 0, user, true);
					cart = cartService.saveCart(cart);
				}
				CartItem cartItem = cartService.getCartItemByPidAndCid(product, cart);
				if (cartItem == null) {
					if (request.getQuantity() <= product.getInventory()) {
						cartItem = new CartItem(cart, product, request.getQuantity());
						cartItem.setCart(cart);
						cartService.saveCartItem(cartItem);
						return new DataResponse("200", "Thêm sản phẩm vào giỏ hàng thành công", 200);
					}
				} else {
					if (cartItem.getQuantity() + request.getQuantity() <= product.getInventory()) {
						cartItem.setQuantity(cartItem.getQuantity() + request.getQuantity());
						cartItem.setCart(cart);
						cartService.saveCartItem(cartItem);
						return new DataResponse("200", "Tăng số lượng sản phẩm vào giỏ hàng thành công", 200);
					}
				}
				return new DataResponse("200", "Số lượng quá lớn, vui lòng kiểm tra lại", 200);
			} 
			else
				throw new RuntimeException("User khong ton tai");
		} else
			throw new RuntimeException("Access token missing");

	}

	@PutMapping("/sub")
	@PreAuthorize("hasRole('USER')")
	public DataResponse subItem(@RequestBody @Valid SubItemRequest request, HttpServletRequest httpServletRequest,
			BindingResult result) {
		String AuthorizationHeader = httpServletRequest.getHeader("Authorization");
		if (AuthorizationHeader != null && AuthorizationHeader.startsWith("Bearer ")) {
			String accessToken = AuthorizationHeader.substring("Bearer ".length());
			String username = tokenProvider.getUsernameFromJWT(accessToken);
			User user = userRepository.findByUsername(username).get();
			if (user != null) {
				Cart cart = cartService.getCartByUid(user);
				if (cart == null) {
					return new DataResponse("400", "nguoi dung chưa có cart", 200);
				}
				CartItem cartItem = cartService.getItemByIdAndCart(request.getId(), cart);
				if (cartItem == null) {
					return new DataResponse("400", "khong tim thay sản phẩm vào giỏ hàng của bạn", 200);
				}
				if (request.getQuantity() >= cartItem.getQuantity()) {
					cartService.deleteCartItem(cartItem.getId());
					return new DataResponse("200", "Item đã được xóa", 200);
				} else {
					cartItem.setQuantity(cartItem.getQuantity() - request.getQuantity());
					cartService.saveCartItem(cartItem);
					return new DataResponse("200", "Giảm số lương thành công", 200);
				}

			} else
				throw new RuntimeException("User khong ton tai");
		} else
			throw new RuntimeException("Access token missing");
	}

}

package com.fsoft.intern.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fsoft.intern.model.entity.User;
import com.fsoft.intern.model.request.AddressRequest;
import com.fsoft.intern.model.response.DataResponse;
import com.fsoft.intern.repository.UserRepository;
import com.fsoft.intern.security.JwtTokenProvider;
import com.fsoft.intern.service.AddressService;
import com.fsoft.intern.service.UserService;

@RestController
@RequestMapping(path = "/api/v1/address")
public class UserAddressController {
	@Autowired
	private UserService userService;
	@Autowired
	private JwtTokenProvider tokenProvider;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private AddressService addressService;

	@GetMapping("")
	public DataResponse getListAddress(HttpServletRequest httpServletRequest) {
		String AuthorizationHeader = httpServletRequest.getHeader("Authorization");
		if (AuthorizationHeader != null && AuthorizationHeader.startsWith("Bearer ")) {
			String accessToken = AuthorizationHeader.substring("Bearer ".length());
			String username = tokenProvider.getUsernameFromJWT(accessToken);
			User user = userRepository.findByUsername(username).get();
			if (user != null) {
				return new DataResponse(addressService.listAllbyUid(user.getId()));
			} else
				throw new RuntimeException("User khong ton tai");
		} else
			throw new RuntimeException("Access token missing");
	}

	@PostMapping("")
	public DataResponse addAddress(@RequestBody @Valid AddressRequest request, BindingResult result,
			HttpServletRequest httpServletRequest) {
		if (!result.hasErrors()) {
			String AuthorizationHeader = httpServletRequest.getHeader("Authorization");
			if (AuthorizationHeader != null && AuthorizationHeader.startsWith("Bearer ")) {
				String accessToken = AuthorizationHeader.substring("Bearer ".length());
				String username = tokenProvider.getUsernameFromJWT(accessToken);
				User user = userRepository.findByUsername(username).get();
				if (user != null) {
					return new DataResponse(addressService.addAddress(request, user.getId()));
				} else
					throw new RuntimeException("User khong ton tai");
			} else
				throw new RuntimeException("Access token missing");
		}
		throw new RuntimeException(result.getFieldError().toString());
	}

	@PutMapping("")
	public DataResponse updateAddress(@PathVariable int id, @RequestBody @Valid AddressRequest request,
			BindingResult result, HttpServletRequest httpServletRequest) {
		if (!result.hasErrors()) {
			String AuthorizationHeader = httpServletRequest.getHeader("Authorization");
			if (AuthorizationHeader != null && AuthorizationHeader.startsWith("Bearer ")) {
				String accessToken = AuthorizationHeader.substring("Bearer ".length());
				String username = tokenProvider.getUsernameFromJWT(accessToken);
				User user = userRepository.findByUsername(username).get();
				if (user != null) {
					return new DataResponse(addressService.editAddress(id, request));
				} else
					throw new RuntimeException("User khong ton tai");
			} else
				throw new RuntimeException("Access token missing");
		}
		throw new RuntimeException(result.getFieldError().toString());

	}

}

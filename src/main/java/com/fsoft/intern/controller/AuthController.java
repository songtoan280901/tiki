package com.fsoft.intern.controller;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.xml.crypto.Data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fsoft.intern.model.request.LoginRequest;

import com.fsoft.intern.model.request.SignUpRequest;
import com.fsoft.intern.model.request.SignUpSellerRequest;
import com.fsoft.intern.model.response.JWTAuthResponse;

import com.fsoft.intern.repository.UserRepository;
import com.fsoft.intern.security.JwtTokenProvider;
import com.fsoft.intern.service.UserService;
import com.fsoft.intern.security.CustomUserDetailsService;

@RestController
@RequestMapping("/api/v1/auth")

public class AuthController {
	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserDetailsService jwtInMemoryUserDetailsService;
	@Autowired
	private JwtTokenProvider tokenProvider;
	@Autowired
	private CustomUserDetailsService userDetailsService;

	@PostMapping("/authenticateuser")
	public ResponseEntity<?> authenticateUser(@RequestBody LoginRequest loginDto) throws Exception {

		authenticate(loginDto.getUsername(), loginDto.getPassword());

		final UserDetails userDetails = jwtInMemoryUserDetailsService.loadUserByUsername(loginDto.getUsername());

		final String token = tokenProvider.generateToken(userDetails);
		return ResponseEntity.ok(new JWTAuthResponse(token));

	}

	@PostMapping("/signup")
	public ResponseEntity<?> saveUser(@RequestBody @Valid SignUpRequest user) throws Exception {
		// add check for username exists in a DB
		if (userRepository.existsByUsername(user.getUsername())) {
			return new ResponseEntity<>("Username is already taken!", HttpStatus.BAD_REQUEST);
		}

		// add check for email exists in DB
		if (userRepository.existsByEmail(user.getEmail())) {
			return new ResponseEntity<>("Email is already taken!", HttpStatus.BAD_REQUEST);
		}
		return ResponseEntity.ok(userDetailsService.save(user));
	}

	private void authenticate(String username, String password) throws Exception {
		Objects.requireNonNull(username);
		Objects.requireNonNull(password);

		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}

}

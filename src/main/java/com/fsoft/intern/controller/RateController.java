package com.fsoft.intern.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fsoft.intern.model.entity.Rate;
import com.fsoft.intern.model.entity.User;
import com.fsoft.intern.model.request.RateRequest;
import com.fsoft.intern.model.response.DataResponse;
import com.fsoft.intern.repository.UserRepository;
import com.fsoft.intern.security.JwtTokenProvider;
import com.fsoft.intern.service.RateService;

@RestController
@RequestMapping(path = "/api/v1/rate")
public class RateController {
	@Autowired
	private RateService rateService;
	@Autowired
	private JwtTokenProvider tokenProvider;
	@Autowired
	private UserRepository userRepository;

	@PostMapping("")
	public DataResponse addRate(@RequestBody RateRequest dto, HttpServletRequest httpServletRequest,
			BindingResult result) {
		if (!result.hasErrors()) {
			String AuthorizationHeader = httpServletRequest.getHeader("Authorization");
			if (AuthorizationHeader != null && AuthorizationHeader.startsWith("Bearer ")) {
				String accessToken = AuthorizationHeader.substring("Bearer ".length());
				String username = tokenProvider.getUsernameFromJWT(accessToken);
				User user = userRepository.findByUsername(username).get();
				if (user != null) {
					return new DataResponse(rateService.saveRate(dto, user.getId()));
				} else
					throw new RuntimeException("User khong ton tai");
			} else
				throw new RuntimeException("Access token missing");
		}
		throw new RuntimeException(result.getFieldError().toString());
	}

	@GetMapping("/product/{id}")
	public Page<Rate> getNewestProductsByCate(@PathVariable int id,
			@RequestParam(required = false, defaultValue = "1") Integer pageNumber,
			@RequestParam(required = false, defaultValue = "5") Integer pageSize) {
		return rateService.getRatetByPid(id, pageNumber, pageSize);
	}
}

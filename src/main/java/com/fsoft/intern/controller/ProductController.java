package com.fsoft.intern.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fsoft.intern.model.entity.Product;
import com.fsoft.intern.model.entity.User;
import com.fsoft.intern.model.request.ProductRequest;
import com.fsoft.intern.model.request.SearchProductRequest;
import com.fsoft.intern.model.response.DataResponse;
import com.fsoft.intern.model.response.ResponseObject;
import com.fsoft.intern.repository.ProductRepository;
import com.fsoft.intern.repository.UserRepository;
import com.fsoft.intern.security.JwtTokenProvider;
import com.fsoft.intern.service.ProductService;

@RestController
@RequestMapping(path = "/api/v1/Products")
public class ProductController {
	@Autowired
	private ProductRepository productRepository;
	@Autowired
	private ProductService productService;
	@Autowired
	private JwtTokenProvider tokenProvider;
	@Autowired
	private UserRepository userRepository;

	@GetMapping("")
	List<Product> getAllProducts() {
		return productRepository.findAll();
	}

	@GetMapping("/{id}")
	ResponseEntity<ResponseObject> findById(@PathVariable Integer id) {
		Optional<Product> foundProduct = productRepository.findById(id);
		if (foundProduct.isPresent()) {
			return ResponseEntity.status(HttpStatus.OK)
					.body(new ResponseObject("ok", "Query product successfully", foundProduct));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body(new ResponseObject("false", "Cannot find product with id = " + id, ""));
		}
	}

	@GetMapping("/search")
	public ResponseEntity<List<Product>> getProductByName(@RequestParam String name) {
		return new ResponseEntity<List<Product>>(productService.productSearchByName(name), HttpStatus.OK);
	}

	@GetMapping("/category/{cate_id}")
	public ResponseEntity<List<Product>> getProductByCid(@PathVariable int cate_id) {
		return new ResponseEntity<List<Product>>(productService.listProductsByCid(cate_id), HttpStatus.OK);
	}

	@GetMapping("/brand/{cate_id}/{brand_id}")
	public ResponseEntity<List<Product>> getProductByBid(@PathVariable int cate_id, @PathVariable int brand_id) {
		return new ResponseEntity<List<Product>>(productService.listProductsByBid(cate_id, brand_id), HttpStatus.OK);
	}

	@PutMapping("/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public Product updateProduct(@PathVariable int id, @RequestBody ProductRequest product) {
		return productService.updateProduct(product, id);
	}

	@DeleteMapping("/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	ResponseEntity<ResponseObject> deleteProductById(@PathVariable Integer id) {
		boolean exists = productRepository.existsById(id);
		if (exists) {
			productRepository.deleteById(id);
			return ResponseEntity.status(HttpStatus.OK)
					.body(new ResponseObject("ok", "Delete product successfully", ""));
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.body(new ResponseObject("failed", "Cannot find product to delete", ""));
	}

	@PostMapping("")
	@PreAuthorize("hasRole('ADMIN')")
	public DataResponse addProduct(@RequestBody @Valid ProductRequest newProduct, BindingResult result,
			HttpServletRequest httpServletRequest) {
		if (!result.hasErrors()) {
			String AuthorizationHeader = httpServletRequest.getHeader("Authorization");
			if (AuthorizationHeader != null && AuthorizationHeader.startsWith("Bearer ")) {
				String accessToken = AuthorizationHeader.substring("Bearer ".length());
				String username = tokenProvider.getUsernameFromJWT(accessToken);
				User user = userRepository.findByUsername(username).get();
				if (user != null) {
					return new DataResponse(productService.saveProduct(newProduct, user.getId()));
				} else
					throw new RuntimeException("User khong ton tai");
			} else
				throw new RuntimeException("Access token missing");
		}
		throw new RuntimeException(result.getFieldError().toString());
	}

	/// Ham nay can sua lai ne
	@RequestMapping(value = "/{cate_id}/paging", method = RequestMethod.GET)
	public Page<Product> productsPagination(@PathVariable int cate_id,
			@RequestParam(required = false, defaultValue = "1") Integer pageNumber,
			@RequestParam(required = false, defaultValue = "5") Integer pageSize) {
		return productService.listProductsWithPagination(cate_id, pageNumber, pageSize);
	}

	@RequestMapping(value = "/{cate_id}/pagingAndSorting/priceASC", method = RequestMethod.GET)
	public Page<Product> productsPaginationWithPriceASC(@PathVariable int cate_id,
			@RequestParam(required = false, defaultValue = "1") Integer pageNumber,
			@RequestParam(required = false, defaultValue = "5") Integer pageSize) {
		return productService.listProductsWithPaginationAndPriceASC(cate_id, pageNumber, pageSize);
	}

	@RequestMapping(value = "/{cate_id}/pagingAndSorting/priceDESC", method = RequestMethod.GET)
	public Page<Product> productsPaginationWithPriceDESC(@PathVariable int cate_id,
			@RequestParam(required = false, defaultValue = "1") Integer pageNumber,
			@RequestParam(required = false, defaultValue = "5") Integer pageSize) {
		return productService.listProductsWithPaginationAndPriceDESC(cate_id, pageNumber, pageSize);
	}

	@GetMapping("/filteredByPrice/{cate_id}/{priceMin}/{priceMax}")
	public List<Product> getProductsBetween(@PathVariable int cate_id, @PathVariable int priceMin,
			@PathVariable int priceMax) {
		return productService.getProductsBetween(cate_id, priceMin, priceMax);
	}

	@RequestMapping(value = "/{cate_id}/pagingAndSorting/newest", method = RequestMethod.GET)
	public Page<Product> getNewestProducts(@PathVariable int cate_id,
			@RequestParam(required = false, defaultValue = "1") Integer pageNumber,
			@RequestParam(required = false, defaultValue = "5") Integer pageSize) {
		return productService.listNewestProductsWithPagination(cate_id, pageNumber, pageSize);
	}

	@RequestMapping(value = "/{cate_id}/newestProductsByCate", method = RequestMethod.GET)
	public Page<Product> getNewestProductsByCate(@PathVariable int cate_id,
			@RequestParam(required = false, defaultValue = "1") Integer pageNumber,
			@RequestParam(required = false, defaultValue = "5") Integer pageSize) {
		return productService.listNewestProductsByCate(cate_id, pageNumber, pageSize);
	}

	@RequestMapping(value = "/{cate_id}/ratingFrom3", method = RequestMethod.GET)
	public Page<Product> listProductByRateFrom3(@PathVariable int cate_id,
			@RequestParam(required = false, defaultValue = "1") Integer pageNumber,
			@RequestParam(required = false, defaultValue = "5") Integer pageSize) {
		return productService.listProductsByRateFrom3(cate_id, pageNumber, pageSize);
	}

	@RequestMapping(value = "/{cate_id}/ratingFrom4", method = RequestMethod.GET)
	public Page<Product> listProductByRateFrom4(@PathVariable int cate_id,
			@RequestParam(required = false, defaultValue = "1") Integer pageNumber,
			@RequestParam(required = false, defaultValue = "5") Integer pageSize) {
		return productService.listProductsByRateFrom3(cate_id, pageNumber, pageSize);
	}

	@RequestMapping(value = "/{cate_id}/ratingFrom5", method = RequestMethod.GET)
	public Page<Product> listProductByRateFrom5(@PathVariable int cate_id,
			@RequestParam(required = false, defaultValue = "1") Integer pageNumber,
			@RequestParam(required = false, defaultValue = "5") Integer pageSize) {
		return productService.listProductsByRateFrom3(cate_id, pageNumber, pageSize);
	}
}

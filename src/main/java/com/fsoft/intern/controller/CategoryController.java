package com.fsoft.intern.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fsoft.intern.model.entity.Category;
import com.fsoft.intern.model.request.AddCategoryRequest;
import com.fsoft.intern.model.response.DataResponse;
import com.fsoft.intern.service.CategoryService;

@RestController
@RequestMapping(value = "/api/v1/category")
public class CategoryController {
	@Autowired
	private CategoryService categoryService;

	@PostMapping("")
	@PreAuthorize("hasRole('ADMIN')")
	public DataResponse addCategory(@RequestBody @Valid AddCategoryRequest category, BindingResult result) {
		if (!result.hasErrors()) {
			return new DataResponse(categoryService.addCategory(category));
		} else {
			throw new RuntimeException(result.getFieldError().toString());
		}
	}

	// Cap nhat Category
	@PutMapping(value = "/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public Category updateCategory(@RequestParam("id") int id, @RequestBody AddCategoryRequest category) {
		return categoryService.updateCategory(id, category);

	}

	// Xoa Category
	@DeleteMapping(value = "/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public boolean deleteCategory(@RequestParam("id") int id) {
		return categoryService.deleteCategory(id);

	}

	// Lay toan bo Category
	@GetMapping("")
	public DataResponse getAllCategory() {
		return new DataResponse(categoryService.getAllCategory());

	}
}

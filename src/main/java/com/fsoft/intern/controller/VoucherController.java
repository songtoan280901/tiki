package com.fsoft.intern.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fsoft.intern.model.entity.Vouchers;
import com.fsoft.intern.model.request.VoucherRequest;
import com.fsoft.intern.model.response.DataResponse;
import com.fsoft.intern.service.UserService;
import com.fsoft.intern.service.VoucherService;

@RestController
@RequestMapping("/api/v1/voucher")
public class VoucherController {

	@Autowired
	private VoucherService voucherService;
	@Autowired
	private UserService userService;

	@GetMapping("/all")
	public DataResponse showAllVoucher() {
		List<Vouchers> listVoucher = voucherService.findAllVoucher();
		if (listVoucher.size() == 0) {
			return new DataResponse("400", "Voucher is empty", 200);
		}
		return new DataResponse(listVoucher);
	}

	@PostMapping("/insert")
	@PreAuthorize("hasRole('ADMIN')")
	public DataResponse insertVoucher(@RequestBody Vouchers newVoucher) {
		List<Vouchers> foundVoucher = voucherService.foundVocher(newVoucher.getType());
		if (foundVoucher.size() > 0) {
			return new DataResponse("400", "Voucher found", 200);
		}
		return new DataResponse(voucherService.save(newVoucher));
	}

	@PutMapping("/update")
	@PreAuthorize("hasRole('ADMIN')")
	public DataResponse updateVoucher(@PathVariable int id, @RequestBody VoucherRequest newVoucher,
			BindingResult result) {
		Vouchers vouchers = voucherService.findbyId(id);
		if (vouchers != null) {
			List<Vouchers> foundVoucher = voucherService.foundVocher(newVoucher.getType());
			if (foundVoucher.size() > 0) {
				return new DataResponse("400", "Voucher found", 200);
			}
			vouchers.setId(id);
			vouchers.setAmount(newVoucher.getAmount());
			vouchers.setCreatedAt(newVoucher.getCreatedAt());
			vouchers.setExpiredDate(newVoucher.getExpiredDate());
			vouchers.setToDate(newVoucher.getToDate());
			return new DataResponse(voucherService.save(vouchers));
		}
		return new DataResponse("400", "Voucher not found", 200);
	}

	@DeleteMapping("")
	public DataResponse deleteVoucher(@PathVariable int id) {
		Vouchers vouchers = voucherService.findbyId(id);
		if (vouchers != null) {
			voucherService.delete(id);
			return new DataResponse("200", "Xóa voucher thành công", 200);
		}
		return new DataResponse("400", "Voucher không tồn tại", 200); 
	}
}

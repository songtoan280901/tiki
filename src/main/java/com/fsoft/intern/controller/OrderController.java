package com.fsoft.intern.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fsoft.intern.model.ExcelExporter;
import com.fsoft.intern.model.entity.Address;
import com.fsoft.intern.model.entity.Cart;
import com.fsoft.intern.model.entity.CartItem;
import com.fsoft.intern.model.entity.Category;
import com.fsoft.intern.model.entity.Order;
import com.fsoft.intern.model.entity.User;
import com.fsoft.intern.model.request.AddOrderRequest;
import com.fsoft.intern.model.request.CartRequest;
import com.fsoft.intern.model.response.DataResponse;
import com.fsoft.intern.repository.UserRepository;
import com.fsoft.intern.security.JwtTokenProvider;
import com.fsoft.intern.service.AddressService;
import com.fsoft.intern.service.CartService;
import com.fsoft.intern.service.CategoryService;
import com.fsoft.intern.service.OrderService;

@RestController
@RequestMapping(path = "/api/v1/Orders")
public class OrderController {

	@Autowired
	private JwtTokenProvider tokenProvider;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private CartService cartservice;
	@Autowired
	private AddressService addressService;
	@Autowired
	private OrderService orderService;
	@Autowired
	private CategoryService categoryService;

	@PutMapping("")
	@PreAuthorize("hasRole('USER')")
	public DataResponse saveOrder(@RequestBody AddOrderRequest request, HttpServletRequest httpServletRequest,
			BindingResult result) {
		if (!result.hasErrors()) {
			String AuthorizationHeader = httpServletRequest.getHeader("Authorization");
			if (AuthorizationHeader != null && AuthorizationHeader.startsWith("Bearer ")) {
				String accessToken = AuthorizationHeader.substring("Bearer ".length());
				String username = tokenProvider.getUsernameFromJWT(accessToken);
				User user = userRepository.findByUsername(username).get();
				if (user != null) {
					Order order = new Order();
					order.setUserOrder(user);
					Cart cart = cartservice.getCartByUid(user);
					List<CartItem> listPick = new ArrayList<CartItem>();
					List<CartItem> listLeft = new ArrayList<CartItem>();

					listPick = listProductPick(request, cart);
					listLeft = cart.getCartitem();

					cart.setCartitem(listPick);
					cart.setStatus(false);
					order.setCartOrder(cart);

					Cart newCart = new Cart(0.0, user, true);
					newCart.setCartitem(listLeft);

					List<Address> list = user.getAddress();
					Address address = addressService.findById(request.getIdAddress());
					for (Address address1 : list) {
						if (address.getId() == address1.getId()) {
							order.setAddressOrder(address);
							order.setCreatedDate(LocalDateTime.now());

							double totalOrder = PayMoney(listPick);

							cart.setTotal(totalOrder);
							order.setTotal(totalOrder);
							order.setDelStatus(0);
							order.setExpectedDate(LocalDate.from(order.getCreatedDate().plusDays(2)));
							orderService.save(order);
							cartservice.saveCart(newCart);
							return new DataResponse("200", "Add Order Success", 200);
						}
					}
					return new DataResponse("400", "không tìm thấy địa chỉ", 200);
				} else
					throw new RuntimeException("User khong ton tai");
			} else
				throw new RuntimeException("Access token missing");
		}
		throw new RuntimeException(result.getFieldError().toString());
	}

	@GetMapping("")
	@PreAuthorize("hasRole('USER')")
	public DataResponse getOrderByUser(HttpServletRequest httpServletRequest) {
		String AuthorizationHeader = httpServletRequest.getHeader("Authorization");
		if (AuthorizationHeader != null && AuthorizationHeader.startsWith("Bearer ")) {
			String accessToken = AuthorizationHeader.substring("Bearer ".length());
			String username = tokenProvider.getUsernameFromJWT(accessToken);
			User user = userRepository.findByUsername(username).get();
			if (user != null) {
				List<Order> listOrder = user.getOrder();
				if (listOrder.isEmpty()) {
					return new DataResponse("400", "Người dùng chưa có đơn", 200);
				}
				return new DataResponse(listOrder);
			} else
				throw new RuntimeException("User khong ton tai");
		} else
			throw new RuntimeException("Access token missing");
	}

	private List<CartItem> listProductPick(AddOrderRequest request, Cart cart) {
		List<CartItem> list = new ArrayList<CartItem>();
		if (request.getCartItem().length == 0) {
			return null;
		} else {
			for (int i : request.getCartItem()) {
				CartItem cartItem = cartservice.getItemByIdAndCart(i, cart);
				list.add(cartItem);
				cart.getCartitem().remove(cartItem);
			}
		}
		return list;
	}

	private double PayMoney(List<CartItem> list) {
		double totalOrderProduct = 0.0;
		double totalOrder = 0.0;

		for (CartItem item : list) {
			totalOrderProduct += item.getProduct().getPrice() * item.getQuantity();
			item.getProduct().setInventory(item.getProduct().getInventory() - item.getQuantity());

		}
		totalOrder = totalOrderProduct;
		return totalOrder;
	}

	@GetMapping("/export/excel")
	@PreAuthorize("hasRole('ADMIN')")
	public void export(HttpServletResponse response) throws IOException {
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());

		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=orders_" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);

		List<Order> listOrders = orderService.getAll();
		// List<Category> listCate = categoryService.getAllCategory();

		ExcelExporter excelExporter = new ExcelExporter(listOrders);

		excelExporter.export(response);
	}

}

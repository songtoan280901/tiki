package com.fsoft.intern.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fsoft.intern.model.entity.Brand;
import com.fsoft.intern.model.request.BrandRequest;
import com.fsoft.intern.model.response.DataResponse;
import com.fsoft.intern.model.response.ResponseObject;
import com.fsoft.intern.repository.BrandRepository;
import com.fsoft.intern.service.BrandService;

@RestController
@RequestMapping(path = "/api/v1/brands")
public class BrandController {
	@Autowired
	private BrandService brandService;
	@Autowired
	private BrandRepository brandRepository;

	@GetMapping("")
	List<Brand> listAllBrands() {
		return brandService.getAllBrands();
	}

	@PostMapping("")
	@PreAuthorize("hasRole('ADMIN')")
	public DataResponse addBrand(@RequestBody @Valid BrandRequest dto, BindingResult result) {
		if (!result.hasErrors()) {
			return new DataResponse(brandService.addBrand(dto));
		} else {
			throw new RuntimeException(result.getFieldError().toString());
		}
	}

	@PutMapping("/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public Brand updateBrand(@PathVariable int id, @RequestBody BrandRequest brand) {
		return brandService.updateBrand(brand, id);
	}

	@DeleteMapping("/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<ResponseObject> deleteBrandById(@PathVariable int id) {
		boolean exists = brandRepository.existsById(id);
		if (exists) {
			brandService.deleteBrandById(id);
			return ResponseEntity.status(HttpStatus.OK).body(new ResponseObject("ok", "Delete brand successfully", ""));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body(new ResponseObject("failed", "Cannot find brand to delete", ""));
		}
	}
}

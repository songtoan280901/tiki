package com.fsoft.intern.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "\"addresses\"")
public class Address {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "\"id\"")
	private int id;
	@Column(name = "\"fullname\"")
	private String fullname;
	@Column(name = "\"company\"")
	private String company;
	@Column(name = "\"phone\"")
	private String phone;
	@Column(name = "\"detail\"")
	private String detail;
	@OneToOne
	@JoinColumn(name = "province_id")
	private Province province;
	@OneToOne
	@JoinColumn(name = "district_id")
	private District district;
	@OneToOne
	@JoinColumn(name = "commune_id")
	private Commune commune;
	@ManyToOne
	@JoinColumn(name = "\"user_id\"", nullable = false)
	@JsonIgnore
	private User user;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public Province getProvince() {
		return province;
	}

	public void setProvince(Province province) {
		this.province = province;
	}

	public District getDistrict() {
		return district;
	}

	public void setDistrict(District district) {
		this.district = district;
	}

	public Commune getCommune() {
		return commune;
	}

	public void setCommune(Commune commune) {
		this.commune = commune;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Address(int id, String fullname, String company, String phone, String detail, Province province,
			District district, Commune commune, User user) {
		super();
		this.id = id;
		this.fullname = fullname;
		this.company = company;
		this.phone = phone;
		this.detail = detail;
		this.province = province;
		this.district = district;
		this.commune = commune;
		this.user = user;
	}

	public Address() {
		super();
	}

}

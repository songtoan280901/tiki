package com.fsoft.intern.model.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "\"Vouchers\"")
public class Vouchers {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "\"id\"")
	private int id;
	@Column(name = "amount")
	private String amount;

	@Column(name = "type")
	private String type;

	@Column(name = "created_at")
	private Date createdAt;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "from_date")
	private Date fromDate;

	@Column(name = "to_date")
	private Date toDate;

	@Column(name = "expired_date")
	private Date expiredDate;

	@JsonIgnore
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "\"user_voucher\"", joinColumns = @JoinColumn(name = "\"voucher_id\""), inverseJoinColumns = @JoinColumn(name = "\"user_id\""))
	private Set<User> userEntities;

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(Date expiredDate) {
		this.expiredDate = expiredDate;
	}

	public Set<User> getUserEntities() {
		return userEntities;
	}

	public void setUserEntities(Set<User> userEntities) {
		this.userEntities = userEntities;
	}

	public Vouchers(int id, String amount, String type, Date createdAt, Date fromDate, Date toDate, Date expiredDate,
			Set<User> userEntities) {
		super();
		this.id = id;
		this.amount = amount;
		this.type = type;
		this.createdAt = createdAt;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.expiredDate = expiredDate;
		this.userEntities = userEntities;
	}

	public Vouchers() {
		super();
	}

}

package com.fsoft.intern.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "\"product_rates\"")
public class Rate {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "\"id\"")
	private int id;

	@Column(name = "\"rating_point\"")
	private int rating_point;

	@Column(name = "\"message\"")
	private String message;

	@Column(name = "\"date\"")
	private Date date;

	@Column(name = "\"image\"")
	private String image;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User uid;

	@ManyToOne
	@JoinColumn(name = "product_id")
	@JsonIgnore
	private Product pid;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getRating_point() {
		return rating_point;
	}

	public void setRating_point(int rating_point) {
		this.rating_point = rating_point;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date localDate) {
		this.date = localDate;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public User getUid() {
		return uid;
	}

	public void setUid(User uid) {
		this.uid = uid;
	}

	public Product getPid() {
		return pid;
	}

	public void setPid(Product pid) {
		this.pid = pid;
	}

	public Rate() {
		super();
	}

	public Rate(int id, int rating_point, String message, Date date, String image, User uid, Product pid) {
		super();
		this.id = id;
		this.rating_point = rating_point;
		this.message = message;
		this.date = date;
		this.image = image;
		this.uid = uid;
		this.pid = pid;
	}

}

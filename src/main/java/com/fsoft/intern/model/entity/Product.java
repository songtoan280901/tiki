package com.fsoft.intern.model.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "\"Products\"")
public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "\"id\"")
	private int id;
	@Column(name = "\"name\"")
	private String name;
	@Column(name = "\"origin\"")
	private String origin;
	@Column(name = "\"image\"")
	private String image;
	@Column(name = "\"description\"")
	private String description;
	@Column(name = "\"price\"")
	private int price;
	@Column(name = "\"rate_point\"")
	private Integer rate;

	@ManyToOne()
	@JoinColumn(name = "cate_id")
	@JsonIgnore
	private Category category;

	@ManyToOne()
	@JoinColumn(name = "user_id")
	@JsonIgnore
	private User user;

	@OneToMany(mappedBy = "product", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonIgnore
	List<CartItem> cart;

	@ManyToOne()
	@JoinColumn(name = "brand_id")
	@JsonIgnore
	private Brand brand;
	@OneToMany(mappedBy = "pid", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonIgnore
	List<Rate> rates;
	@Column(name = "\"inventory\"")
	private int inventory;

	public int getInventory() {
		return inventory;
	}

	public void setInventory(int inventory) {
		this.inventory = inventory;
	}

	public Integer getRate() {
		return rate;
	}

	public void setRate(Integer rate) {
		this.rate = rate;
	}

	public List<Rate> getRates() {
		return rates;
	}

	public void setRates(List<Rate> rates) {
		this.rates = rates;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public Product() {
		super();
	}

	public Product(int id, String name, String origin, String image, String description, int price, Integer rate,
			Category category, User user, List<CartItem> cart, Brand brand, List<Rate> rates, int inventory) {
		super();
		this.id = id;
		this.name = name;
		this.origin = origin;
		this.image = image;
		this.description = description;
		this.price = price;
		this.rate = rate;
		this.category = category;
		this.user = user;
		this.cart = cart;
		this.brand = brand;
		this.rates = rates;
		this.inventory = inventory;
	}

	public List<CartItem> getCart() {
		return cart;
	}

	public void setCart(List<CartItem> cart) {
		this.cart = cart;
	}

	public Brand getBrand() {
		return brand;
	}

	public void setBrand(Brand brand) {
		this.brand = brand;
	}

}

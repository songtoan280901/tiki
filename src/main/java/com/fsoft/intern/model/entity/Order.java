package com.fsoft.intern.model.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "\"Orders\"")
public class Order {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "\"id\"")
	private int id;

	@ManyToOne()
	@JsonIgnore
	@JoinColumn(name = "\"user_id\"")
	private User userOrder;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "\"cart_id\"")
	private Cart cartOrder;


	@ManyToOne()
	@JsonIgnore
	@JoinColumn(name = "\"user_address\"")
	private Address addressOrder;

	@Column(name = "\"created_date\"")
	private LocalDateTime createdDate;

	@Column(name = "\"total\"")
	private double total;

	@Column(name = "\"del_status\"")
	private int delStatus;

	@Column(name = "\"expected_date\"")
	private LocalDate expectedDate;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUserOrder() {
		return userOrder;
	}

	public void setUserOrder(User userOrder) {
		this.userOrder = userOrder;
	}

	public Cart getCartOrder() {
		return cartOrder;
	}

	public void setCartOrder(Cart cartOrder) {
		this.cartOrder = cartOrder;
	}


	public Address getAddressOrder() {
		return addressOrder;
	}

	public void setAddressOrder(Address addressOrder) {
		this.addressOrder = addressOrder;
	}

	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public int getDelStatus() {
		return delStatus;
	}

	public void setDelStatus(int delStatus) {
		this.delStatus = delStatus;
	}

	public LocalDate getExpectedDate() {
		return expectedDate;
	}

	public void setExpectedDate(LocalDate expectedDate) {
		this.expectedDate = expectedDate;
	}
	
}

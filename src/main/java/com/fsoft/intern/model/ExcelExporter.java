package com.fsoft.intern.model;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.fsoft.intern.model.entity.Category;
import com.fsoft.intern.model.entity.Order;

public class ExcelExporter {
	private XSSFWorkbook workbook;
	private XSSFSheet sheet;
	private List<Order> listOrders;
	private List<Category> listCategories;

	public ExcelExporter(List<Order> listOrders) {
		this.listOrders = listOrders;
		workbook = new XSSFWorkbook();
	}

	private void writeHeaderLine() {
		sheet = workbook.createSheet("Categories");
		Row row = sheet.createRow(0);
		CellStyle style = workbook.createCellStyle();
		XSSFFont font = workbook.createFont();
		font.setBold(true);
		font.setFontHeight(16);
		style.setFont(font);

		createCell(row, 0, "ID", style);

		createCell(row, 1, "Del Status", style);
		createCell(row, 2, "Total", style);
		createCell(row, 3, "User Address", style);
		createCell(row, 4, "Cart ID", style);
		createCell(row, 5, "User name", style);

//		createCell(row, 0, "ID", style);
//		createCell(row, 1, "Name", style);
	}

	private void createCell(Row row, int columnCount, Object value, CellStyle style) {
		sheet.autoSizeColumn(columnCount);
		Cell cell = row.createCell(columnCount);
		if (value instanceof Integer) {
			cell.setCellValue((int) value);
		} else if (value instanceof Boolean) {
			cell.setCellValue((Boolean) value);
		}
		if (value instanceof Double) {
			cell.setCellValue((String) value);
		} else {
			cell.setCellValue((String) value);
		}
		cell.setCellStyle(style);
	}

	private void writeDataLines() {
		int rowCount = 1;

		CellStyle style = workbook.createCellStyle();
		XSSFFont font = workbook.createFont();
		font.setFontHeight(14);
		style.setFont(font);

		for (Order order : listOrders) {
			Row row = (Row) sheet.createRow(rowCount++);
			int columnCount = 0;
			
			createCell(row, columnCount++, String.valueOf(order.getId()), style);
			createCell(row, columnCount++, String.valueOf(order.getDelStatus()), style);
			createCell(row, columnCount++, String.valueOf(order.getTotal()), style);
			createCell(row, columnCount++, String.valueOf(order.getAddressOrder().getDetail()), style);
			createCell(row, columnCount++, String.valueOf(order.getCartOrder().getId()), style);
			createCell(row, columnCount++, String.valueOf(order.getUserOrder().getFullname()), style);

		}

//		for (Category category : listCategories) {
//			Row row = (Row) sheet.createRow(rowCount++);
//			int columnCount = 0;
//
//			createCell(row, columnCount++, category.getId(), style);
//			createCell(row, columnCount++, category.getName(), style);
//		}
	}

	public void export(HttpServletResponse response) throws IOException {
		writeHeaderLine();
		writeDataLines();

		ServletOutputStream outputStream = response.getOutputStream();
		workbook.write(outputStream);
		workbook.close();

		outputStream.close();

	}
}

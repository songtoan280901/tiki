package com.fsoft.intern.model.request;

import lombok.Data;

@Data
public class BrandRequest {
	private String name;
}

package com.fsoft.intern.model.request;

import lombok.Data;

@Data
public class AddressRequest {
	private String fullname;
	private String phone;
	private String company;
	private String detail;
	private int province_id;
	private int district_id;
	private int commune_id;
}

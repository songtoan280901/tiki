package com.fsoft.intern.model.request;

import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class EditUserRequest {
	@NotEmpty(message = "Họ và tên không được để trống")
	private String fullname;
	private Date birthday;
	@NotEmpty(message = "giới tính không được để trống")
	private String gender;
	@NotEmpty(message = "Quốc gia không được để trống")
	private String country;
	@NotNull(message = "Vui lòng chọn loại tỉnh thành")
	private int province_id;

}

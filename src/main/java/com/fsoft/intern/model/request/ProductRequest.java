package com.fsoft.intern.model.request;

import lombok.Data;

@Data
public class ProductRequest {

	private String name;
	private String origin;
	private String image;
	private String description;
	private int inventory;
	private int price;
	private int brand_id;
	private int cate_id;
}

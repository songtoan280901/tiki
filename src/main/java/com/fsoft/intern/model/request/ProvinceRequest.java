package com.fsoft.intern.model.request;

import lombok.Data;

@Data
public class ProvinceRequest {
	private String name;
}

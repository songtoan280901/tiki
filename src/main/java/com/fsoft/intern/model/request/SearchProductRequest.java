package com.fsoft.intern.model.request;

import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class SearchProductRequest {
	private int province_id;
	@Size(min = 1, max = 5)
	private int rating;
	private double minprice;
	private double maxprice;
	private String brand_id;
	
}

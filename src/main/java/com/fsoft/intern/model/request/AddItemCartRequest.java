package com.fsoft.intern.model.request;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class AddItemCartRequest {
	int productId;
	@NotNull(message = "Số lượng sản phẩm không được trống")
	@Min(value = 1, message = "Số lượng sản phẩm phải lớn hơn 1")
	int quantity;

}

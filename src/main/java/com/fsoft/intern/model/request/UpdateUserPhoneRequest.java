package com.fsoft.intern.model.request;

import lombok.Data;

@Data
public class UpdateUserPhoneRequest {
	private String phone;

}

package com.fsoft.intern.model.request;

import java.util.Date;

import lombok.Data;

@Data
public class RateRequest {
	private int rating_point;
	private String message;
	private String image;
	private Date date;
	private int pid;
}

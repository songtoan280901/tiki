package com.fsoft.intern.model.request;

import java.util.Date;

import lombok.Data;

@Data
public class VoucherRequest {

	private String amount;

	private String type;

	private Date createdAt;

	private Date fromDate;

	private Date toDate;

	private Date expiredDate;

}

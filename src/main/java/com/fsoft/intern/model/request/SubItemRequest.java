package com.fsoft.intern.model.request;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class SubItemRequest {
	@NotNull(message = "Id không được để trống")
	private int id;
	@NotNull(message = "Số lượng không được để trống")
	@Min(value = 1)
	private int quantity;
}

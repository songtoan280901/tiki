package com.fsoft.intern.model.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class AddOrderRequest {
	@NotNull(message = "Nhập id địa chỉ")
	private int idAddress;
	private int[] cartItem;
}

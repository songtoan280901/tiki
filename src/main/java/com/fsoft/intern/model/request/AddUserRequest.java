package com.fsoft.intern.model.request;

import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fsoft.intern.model.request.AddUserRequest;

import lombok.Data;

@Data
public class AddUserRequest {
	@NotEmpty(message = "Họ và tên không được để trống")
	private String fullname;
	//@NotEmpty(message = "ngày sinh không được để trống")
	
	private Date birthday;
	@NotEmpty(message = "giới tính không được để trống")
	private String gender;
	@NotEmpty(message = "Số điện thoại không được để trống")
	private String phone;
}

package com.fsoft.intern.model.request;

import lombok.Data;

@Data
public class CartRequest {
	private int product_id;
	private int quantity;
	private String status;
}

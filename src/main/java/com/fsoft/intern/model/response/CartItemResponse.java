package com.fsoft.intern.model.response;

import com.fsoft.intern.model.entity.Product;

import lombok.Data;

@Data
public class CartItemResponse {
	private int id;
	private Product productResponse;
	private int cartId;
	private int quantity;

	public CartItemResponse(int id, Product productResponse, int cartId, int quantity) {
		this.id = id;
		this.productResponse = productResponse;
		this.cartId = cartId;
		this.quantity = quantity;

	}
}

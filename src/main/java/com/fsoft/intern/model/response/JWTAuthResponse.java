package com.fsoft.intern.model.response;

public class JWTAuthResponse {
	private final String jwttoken;

	public JWTAuthResponse(String jwttoken) {
		this.jwttoken = jwttoken;
	}

	public String getToken() {
		return this.jwttoken;
	}
}

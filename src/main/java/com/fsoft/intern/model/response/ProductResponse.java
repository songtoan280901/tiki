package com.fsoft.intern.model.response;

import lombok.Data;

@Data
public class ProductResponse {
	private int id;
	private String name;
	private String origin;
	private String image;
	private String description;
	private int price;
	private int rate;

	public ProductResponse(String name, String origin, String image, String description, int price) {
		super();
		this.name = name;
		this.origin = origin;
		this.image = image;
		this.description = description;
		this.price = price;
	}

}

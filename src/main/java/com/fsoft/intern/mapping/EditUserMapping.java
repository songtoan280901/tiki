package com.fsoft.intern.mapping;

import org.springframework.stereotype.Component;

import com.fsoft.intern.model.entity.User;
import com.fsoft.intern.model.request.EditUserRequest;

@Component
public class EditUserMapping {
	public User userDtoToUser(EditUserRequest dto) {
		if (dto == null) {
			return null;
		}

		User user = new User();

		user.setFullname(dto.getFullname());
		user.setBirthday(dto.getBirthday());
		user.setGender(dto.getGender());

		return user;
	}

	public EditUserRequest UserToUserDTO(User user) {
		if (user == null) {
			return null;
		}

		EditUserRequest editUserRequest = new EditUserRequest();

		editUserRequest.setFullname(user.getFullname());
		editUserRequest.setBirthday(user.getBirthday());
		editUserRequest.setGender(user.getGender());

		return editUserRequest;
	}
}

package com.fsoft.intern.mapping;

import org.springframework.stereotype.Component;

import com.fsoft.intern.model.entity.Brand;
import com.fsoft.intern.model.request.BrandRequest;

@Component
public class BrandMapping {

	public BrandRequest entityToDTO(Brand brand) {
		// TODO Auto-generated method stub
		BrandRequest dto = new BrandRequest();
		dto.setName(brand.getName());
		return dto;
	}


	public Brand dtoToEntiry(BrandRequest dto) {
		// TODO Auto-generated method stub
		Brand entity = new Brand();
		entity.setName(dto.getName());
		return entity;
	}
}

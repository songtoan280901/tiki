package com.fsoft.intern.mapping;

import org.springframework.stereotype.Component;

import com.fsoft.intern.model.entity.Rate;
import com.fsoft.intern.model.request.RateRequest;

@Component
public class RateMapping {
	public RateRequest entityToDto(Rate rate) {
		RateRequest dto = new RateRequest();
		dto.setRating_point(rate.getRating_point());
		dto.setMessage(rate.getMessage());
		dto.setDate(rate.getDate());
		dto.setImage(rate.getImage());
		return dto;
	}

	public Rate dtotoEntity(RateRequest dto) {
		Rate rate = new Rate();
		rate.setRating_point(dto.getRating_point());
		rate.setMessage(dto.getMessage());
		rate.setDate(dto.getDate());
		rate.setImage(dto.getImage());
		return rate;
	}
}

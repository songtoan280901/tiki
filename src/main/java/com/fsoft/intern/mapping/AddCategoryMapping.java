package com.fsoft.intern.mapping;

import org.springframework.stereotype.Component;

import com.fsoft.intern.model.entity.Category;
import com.fsoft.intern.model.request.AddCategoryRequest;

@Component
public class AddCategoryMapping {

	public AddCategoryRequest CatetoDTO(Category cate) {
		AddCategoryRequest dto = new AddCategoryRequest();
		dto.setName(cate.getName());
		return dto;
	}

	public Category DTOtoCate(AddCategoryRequest dto) {
		Category cate = new Category();
		cate.setName(dto.getName());
		return cate;
	}

}

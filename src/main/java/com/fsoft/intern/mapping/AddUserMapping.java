package com.fsoft.intern.mapping;

import org.springframework.stereotype.Component;

import com.fsoft.intern.model.entity.User;
import com.fsoft.intern.model.request.AddUserRequest;
@Component
public class AddUserMapping {
	public User userDtoToUser(AddUserRequest dto) {
		if (dto == null) {
			return null;
		}

		User user = new User();

		user.setFullname(dto.getFullname());
		user.setBirthday(dto.getBirthday());
		user.setGender(dto.getGender());
		user.setPhone(dto.getPhone());
		

		return user;
	}

	public AddUserRequest UserToUserDTO(User user) {
		if (user == null) {
			return null;
		}

		AddUserRequest addUserRequest = new AddUserRequest();

		addUserRequest.setFullname(user.getFullname());
		addUserRequest.setBirthday(user.getBirthday());
		addUserRequest.setGender(user.getGender());
		addUserRequest.setPhone(user.getPhone());

		return addUserRequest;
	}
}

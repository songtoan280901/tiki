package com.fsoft.intern.mapping;

import org.springframework.stereotype.Component;

import com.fsoft.intern.model.entity.Address;

import com.fsoft.intern.model.request.AddressRequest;

@Component
public class AddressMapping {
	public AddressRequest AddtoDTO(Address address) {
		AddressRequest dto = new AddressRequest();
		dto.setCompany(address.getCompany());
		dto.setDetail(address.getDetail());
		dto.setFullname(address.getFullname());
		dto.setPhone(address.getPhone());
		return dto;
	}

	public Address DTOtoAdd(AddressRequest dto) {
		Address address = new Address();
		address.setCompany(dto.getCompany());
		address.setDetail(dto.getDetail());
		address.setFullname(dto.getFullname());
		address.setPhone(dto.getPhone());
		return address;
	}
}

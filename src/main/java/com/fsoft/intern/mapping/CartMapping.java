package com.fsoft.intern.mapping;

import org.springframework.stereotype.Component;

import com.fsoft.intern.model.entity.CartItem;
import com.fsoft.intern.model.request.CartRequest;

@Component
public class CartMapping {
	public CartRequest entityToDTO(CartItem cart) {
		// TODO Auto-generated method stub
		CartRequest dto = new CartRequest();
		dto.setQuantity(cart.getQuantity());
		return dto;
	}

	public CartItem dtoToEntity(CartRequest dto) {
		// TODO Auto-generated method stub
		CartItem cart = new CartItem();
		cart.setQuantity(dto.getQuantity());
		return cart;
	}
}

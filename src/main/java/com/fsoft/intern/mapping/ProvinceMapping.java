package com.fsoft.intern.mapping;

import org.springframework.stereotype.Component;
import com.fsoft.intern.model.entity.Province;
import com.fsoft.intern.model.request.ProvinceRequest;

@Component
public class ProvinceMapping {
	public ProvinceRequest entityToDTO(Province province) {
		// TODO Auto-generated method stub
		ProvinceRequest dto = new ProvinceRequest();
		dto.setName(province.getName());
		return dto;
	}

	public Province dtoToEntity(ProvinceRequest dto) {
		// TODO Auto-generated method stub
		Province province = new Province();
		province.setName(dto.getName());
		return province;
	}
}

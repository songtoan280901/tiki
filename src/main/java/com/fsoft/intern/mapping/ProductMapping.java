package com.fsoft.intern.mapping;

import org.springframework.stereotype.Component;

import com.fsoft.intern.model.entity.Product;
import com.fsoft.intern.model.request.ProductRequest;

@Component
public class ProductMapping {
	public ProductRequest enitytoDto(Product product) {
		ProductRequest dto = new ProductRequest();
		dto.setName(product.getName());
		dto.setDescription(product.getDescription());
		dto.setInventory(product.getInventory());
		dto.setImage(product.getImage());
		dto.setOrigin(product.getOrigin());
		dto.setPrice(product.getPrice());
		return dto;
	}

	public Product DtoToEntity(ProductRequest dto) {
		Product product = new Product();
		product.setName(dto.getName());
		product.setDescription(dto.getDescription());
		product.setInventory(dto.getInventory());
		product.setImage(dto.getImage());
		product.setOrigin(dto.getOrigin());
		product.setPrice(dto.getPrice());
		return product;
	}
}
